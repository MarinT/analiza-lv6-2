﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {

        int i = 0, j = 0, k = 0, ready = 0 , hitCount=0;
        string placeHolder1 = "", placeHolder2 = "";
        char[] textBoxChar, textBoxChar1;
 

        public Form1()
        {
            InitializeComponent();
        }

        private void button_restart(object sender, EventArgs e) // event za gumb koji pokrece ili resetira igru na novu rijec
        {

            label1.Text = "";
            labelGuessesMessage.Text = "Guesses left:";
            labelGuessesCount.Text = "5";

            ready = 1; // oznacava da je igra zapoceta

            string filePath = @"C:\TextFile\wordList.txt"; // cita datoteku koja sadrzi rijeci
            List<string> words = File.ReadAllLines(filePath).ToList(); // stvara listu rijeci citajuci iz datoteke i sprema u words

            textBoxChar1 = words[i].ToCharArray(); // sprema trenutnu rijec u polje tipa char

            if (i < words.Count)
            {
 
                for (j = 0; j < words[i].Length; j++) // for petlja koja broji koliko slova ima u rijeci te za svako slovo postavlja _ i razmak te ispisuje na labeli
                {
                    label1.Text = label1.Text + "_ "; 
                }
                i++;

                
            }
            else
                i = 0;


        }

   
        private void button_guess(object sender, EventArgs e) // event za gumb koji pogadja slovo
        {
            if (ready == 1)
            {
                int hitCountOld = hitCount; // pamti koliko je igrac pogodio rijeci prije ove runde
           placeHolder1 = textBox1.Text; 
            char ch = placeHolder1[0]; // uzima prvi char iz textboxa i ne dozvoljava igracu da se okoristi unosenjem vise charova

            textBoxChar = label1.Text.ToCharArray(); // sprema skrivenu/ poluotkrivenu rijec u polje charova

          
         for (k = 0; k < textBoxChar1.Length; k++) // for petlja koja usporedjuje pogadjanje sa aktualnom rjecju te ukoliko je slovo pogodjeno daje igracu bodove i otkriva slova
            {
                if (textBoxChar1[k] == ch)
                {
                    hitCount++; 
                    textBoxChar[k*2] = ch;
                    label1.Text = new string(textBoxChar);
                              }
                 }

            if (Double.Parse(labelGuessesCount.Text) > 0 && hitCountOld - hitCount == 0) //ukoliko igac nije pogodio slovo oduzima mu jedan "zivot"
                labelGuessesCount.Text = (Double.Parse(labelGuessesCount.Text)-1).ToString();

            if (Double.Parse(labelGuessesCount.Text) == 0) //u slucaju gubitka
            { 


            labelGuessesMessage.Text = "You lose. Womp, womp."; //demoralizirajuca poruka
                labelGuessesCount.Text = "";
                label1.Text = new string(textBoxChar1);

            }

            if (hitCount == textBoxChar1.Length) //u slucaju pobijede
            {
                labelGuessesMessage.Text  = "Well whoopty doo, you've won."; //pasivno-agresivna poruka
                labelGuessesCount.Text = "";
                label1.Text = new string(textBoxChar1);

            }

            textBox1.Clear(); // cisti texbtox za pogadjanje 



                 }
        }

            
    }
}
