﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonGuess = new System.Windows.Forms.Button();
            this.buttonRestart = new System.Windows.Forms.Button();
            this.labelGuessesMessage = new System.Windows.Forms.Label();
            this.labelGuessesCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(40, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(151, 20);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // buttonGuess
            // 
            this.buttonGuess.Location = new System.Drawing.Point(197, 58);
            this.buttonGuess.Name = "buttonGuess";
            this.buttonGuess.Size = new System.Drawing.Size(75, 23);
            this.buttonGuess.TabIndex = 2;
            this.buttonGuess.Text = "guess";
            this.buttonGuess.UseVisualStyleBackColor = true;
            this.buttonGuess.Click += new System.EventHandler(this.button_guess);
            // 
            // buttonRestart
            // 
            this.buttonRestart.Location = new System.Drawing.Point(197, 31);
            this.buttonRestart.Name = "buttonRestart";
            this.buttonRestart.Size = new System.Drawing.Size(75, 23);
            this.buttonRestart.TabIndex = 3;
            this.buttonRestart.Text = "start / restart";
            this.buttonRestart.UseVisualStyleBackColor = true;
            this.buttonRestart.Click += new System.EventHandler(this.button_restart);
            // 
            // labelGuessesMessage
            // 
            this.labelGuessesMessage.AutoSize = true;
            this.labelGuessesMessage.Location = new System.Drawing.Point(37, 9);
            this.labelGuessesMessage.Name = "labelGuessesMessage";
            this.labelGuessesMessage.Size = new System.Drawing.Size(68, 13);
            this.labelGuessesMessage.TabIndex = 4;
            this.labelGuessesMessage.Text = "Guesses left:";
            // 
            // labelGuessesCount
            // 
            this.labelGuessesCount.AutoSize = true;
            this.labelGuessesCount.Location = new System.Drawing.Point(111, 9);
            this.labelGuessesCount.Name = "labelGuessesCount";
            this.labelGuessesCount.Size = new System.Drawing.Size(13, 13);
            this.labelGuessesCount.TabIndex = 5;
            this.labelGuessesCount.Text = "5";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 97);
            this.Controls.Add(this.labelGuessesCount);
            this.Controls.Add(this.labelGuessesMessage);
            this.Controls.Add(this.buttonRestart);
            this.Controls.Add(this.buttonGuess);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonGuess;
        private System.Windows.Forms.Button buttonRestart;
        private System.Windows.Forms.Label labelGuessesMessage;
        private System.Windows.Forms.Label labelGuessesCount;
    }
}

